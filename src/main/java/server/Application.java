package server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by grigory on 11/6/16.
 */
@SpringBootApplication
public class Application{

    public static void main(String[] args)
    {
        SpringApplication.run(Application.class,args);
    }


}
