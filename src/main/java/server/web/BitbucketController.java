package server.web;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import server.service.OAuthBitbucketInterface;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by grigory on 11/6/16.
 */
@RestController
@RequestMapping("/bitbucket")
public class BitbucketController {

    @Autowired
    private OAuthBitbucketInterface BM;


    @RequestMapping(method= RequestMethod.GET)
    public String PrintMessage(HttpServletRequest request, HttpServletResponse httpServletResponse) {

        //http://localhost/bitbucket?oauth_verifier=1086172775&oauth_token=DMPLRHvbhEQ8BzDWTT
        String OAuthVerifier = request.getParameter("oauth_verifier");
        String OAuthToken = request.getParameter("oauth_token");

        String AcToken = BM.GetAcessToken(OAuthVerifier);

        httpServletResponse.setHeader("Location", "/BitbucketResult?"+AcToken);
        httpServletResponse.setStatus(302);
        return AcToken;//request.getHeader(" Authorization ").toString();
    }

    @RequestMapping(value = "/send", method= RequestMethod.GET)
    public String SendPOST(HttpServletResponse httpServletResponse) {
        String url = BM.GetAuthenticateUrl();
        httpServletResponse.setHeader("Location", url);
        httpServletResponse.setStatus(302);
        return url;
    }

    @RequestMapping(value = "/me")
    public void GetData(HttpServletResponse httpServletResponse)
    {
        JSONObject Answer = BM.GetObject("https://api.bitbucket.org/1.0/users/gashetinin");


        JSONObject User = Answer.getJSONObject("user");
        String Username = User.getString("username");
        String FirstName = User.getString("first_name");
        String LastName = User.getString("last_name");

        JSONArray Repositories = Answer.getJSONArray("repositories");
        Integer RepCount = 0;
        if (Repositories != null)
            RepCount = Repositories.length();

        String Data = "username=" + Username + "&firstName=" + FirstName + "&lastName=" + LastName + "&repositoriesCount=" + RepCount.toString();

        httpServletResponse.setHeader("Location", "/BitbucketData/me?"+Data);
        httpServletResponse.setStatus(302);
    }

    @RequestMapping(value = "/repositories", method= RequestMethod.GET)
    public void GetRepositories(HttpServletResponse httpServletResponse) {
        JSONArray Answer = BM.GetArray("https://api.bitbucket.org/1.0/user/repositories");

        String ArrayNames = "RepNames=";

        if (Answer != null)
        for (int i = 0; i < Answer.length(); i++)
        {
            JSONObject object = Answer.getJSONObject(i);
            ArrayNames += object.getString("name")+ "; ";
        }
        else
            ArrayNames = "no repositories available";

        httpServletResponse.setHeader("Location", "/BitbucketData/reps?"+ArrayNames);
        httpServletResponse.setStatus(302);
    }
}
