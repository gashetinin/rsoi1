package server.web;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by grigory on 11/14/16.
 */
@RestController
@RequestMapping("/BitbucketData")
public class BitbucketDataController {
    @RequestMapping(value = "/me", method = RequestMethod.GET)
    public ModelAndView PrintMe()
    {
        ModelAndView model = new ModelAndView("bitbucketDataMe");
        return model;
    }

    @RequestMapping(value = "/reps", method = RequestMethod.GET)
    public ModelAndView PrintReps()
    {
        ModelAndView model = new ModelAndView("bitbucketDataRep");
        return model;
    }

}
