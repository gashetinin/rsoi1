package server.web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by grigory on 11/15/16.
 */
@RestController
@RequestMapping("/BitbucketResult")
public class BitbucketResultController {

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView PrintResult(@RequestParam String oauth_token_secret, @RequestParam String oauth_token)
    {
        ModelAndView model = new ModelAndView("bitbucketResult");
        return model;
    }

}
