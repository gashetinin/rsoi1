package server.web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by grigory on 11/14/16.
 */
@RestController
@RequestMapping("/TwitterData")
public class TwitterDataController {
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView PrintResult(@RequestParam String id,  @RequestParam String name, @RequestParam String createdAt, @RequestParam String statuses_count, @RequestParam String lang)
    {
        ModelAndView model = new ModelAndView("twitterData");
        return model;
    }

}
