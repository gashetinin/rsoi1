package server.web;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import server.service.OAuthTwitterInterface;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by grigory on 11/6/16.
 */
@RestController
@RequestMapping("/twitter")
public class TwitterController {

    @Autowired
    private OAuthTwitterInterface TM;

    @RequestMapping(method= RequestMethod.GET)
    public void PrintMessage(HttpServletRequest request, HttpServletResponse httpServletResponse) {
        String OAuthVerifier = request.getParameter("oauth_verifier");
        String OAuthToken = request.getParameter("oauth_token");

        String AcToken = TM.GetAcessToken(OAuthVerifier);

        httpServletResponse.setHeader("Location", "/TwitterResult?"+AcToken);
        httpServletResponse.setStatus(302);
    }

    @RequestMapping(value = "/me")
    public void GetData(HttpServletResponse httpServletResponse)
    {
        JSONObject Answer = TM.GetObject("https://api.twitter.com/1.1/users/show.json?user_id=795381672850194432");
        String Id = Answer.getString("id_str");
        String Name = Answer.getString("screen_name");
        String CreatedAt = Answer.getString("created_at");
        Integer Statuses = Answer.getInt("statuses_count");
        String Language = Answer.getString("lang");

        String Data = "id="+Id + "&name="+Name + "&createdAt="+CreatedAt + "&statuses_count="+Statuses.toString() + "&lang="+Language;

        httpServletResponse.setHeader("Location", "/TwitterData?"+Data);
        httpServletResponse.setStatus(302);
    }

    @RequestMapping(value = "/postNewTweet/{message}", method= RequestMethod.GET)
    public String PostTweet(@PathVariable String message) {
        String answer = TM.PostTweet("https://api.twitter.com/1.1/statuses/update.json",message.substring(message.indexOf('=')+1));
        return answer;
    }

    @RequestMapping(value = "/send", method= RequestMethod.GET)
    public String SendPOST(HttpServletResponse httpServletResponse) {
        String url = TM.GetRequestToken();
        httpServletResponse.setHeader("Location", url);
        httpServletResponse.setStatus(302);
        return url;
    }
}
