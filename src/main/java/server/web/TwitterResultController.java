package server.web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by grigory on 11/7/16.
 */
@RestController
@RequestMapping("/TwitterResult")
public class TwitterResultController {

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView PrintResult(@RequestParam String oauth_token_secret, @RequestParam String oauth_token)
    {
        ModelAndView model = new ModelAndView("twitterResult");
        return model;
    }

}
