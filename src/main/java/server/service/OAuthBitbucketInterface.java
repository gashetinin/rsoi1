package server.service;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by grigory on 11/7/16.
 */
public interface OAuthBitbucketInterface {

    public String GetAuthenticateUrl();

    public String GetAcessToken(String Verifier);

    public JSONObject GetObject(String adress);

    public JSONArray GetArray(String adress);
}
