package server.service;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import server.model.Data.OAuthConnectionData;
import server.model.Data.OAuthToken;
import server.model.OAuth1Algorithm;

/**
 * Created by grigory on 11/7/16.
 */
@Service
public class OAuth1Bitbucket implements OAuthBitbucketInterface {

    private OAuth1Algorithm algorithm = new OAuth1Algorithm();
    private OAuthConnectionData BitbucketData = new OAuthConnectionData("UtvLkBqNY4cUXfUFaH","T6ZsNBMJw5j6aaArWVFGbWy4NeAJQLBE","http://localhost/bitbucket","https://bitbucket.org/api/1.0/oauth/request_token","https://bitbucket.org/site/oauth1/authorize", "https://bitbucket.org/api/1.0/oauth/access_token");
    private OAuthToken AccessToken;

    public String GetAuthenticateUrl()
    {
        OAuthToken Token = algorithm.FetchRequestToken(BitbucketData.getKey(), BitbucketData.getSecret(), BitbucketData.getCallbackURL(),BitbucketData.getRequestUrl());
        BitbucketData.setRequestToken(Token);
        return algorithm.BuildAuthenticateUrl(BitbucketData.getAuthorizeUrl(),BitbucketData.getRequestToken());
    }

    public String GetAcessToken(String nVerifier)
    {
        AccessToken = algorithm.GetAcessToken(BitbucketData.getKey(), BitbucketData.getSecret(), BitbucketData.getRequestToken(), BitbucketData.getAccessUrl(), nVerifier);
        return "oauth_token_secret="+AccessToken.getSecret()+"&oauth_token="+AccessToken.getValue();
    }

    public JSONObject GetObject(String adress)
    {
        return algorithm.GetRequestObject(adress, BitbucketData.getKey(), BitbucketData.getSecret(), AccessToken.getValue(), AccessToken.getSecret());
    }

    public JSONArray GetArray(String adress)
    {
        return algorithm.GetRequestArray(adress, BitbucketData.getKey(), BitbucketData.getSecret(), AccessToken.getValue(), AccessToken.getSecret());
    }

}
