package server.service;

import org.json.JSONObject;

/**
 * Created by grigory on 11/8/16.
 */
public interface OAuthTwitterInterface {

    public String GetRequestToken();

    public String GetAcessToken(String Verifier);

    public String PostTweet(String adress, String message);

    public JSONObject GetObject(String adress);

}
