package server.model.Data;

/**
 * Created by grigory on 12/29/16.
 */
public class OAuthToken {

    private String value;
    private String secret;

    public OAuthToken(String value, String secret) {
        this.value = value;
        this.secret = secret;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}
