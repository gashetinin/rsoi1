package server.model.Data;


/**
 * Created by grigory on 11/8/16.
 */
public class OAuthConnectionData {

    private String Key;
    private String Secret;
    private String CallbackURL;
    private OAuthToken RequestToken;
    private String RequestUrl;
    private String AuthorizeUrl;
    private String AccessUrl;

    public OAuthConnectionData(String key, String secret, String callbackURL, String requestUrl, String authorizeUrl, String accessUrl) {
        Key = key;
        Secret = secret;
        CallbackURL = callbackURL;
        RequestUrl = requestUrl;
        AuthorizeUrl = authorizeUrl;
        AccessUrl = accessUrl;

        RequestToken = new OAuthToken("","");
    }

    public OAuthConnectionData setRequestToken(OAuthToken requestToken) {
        RequestToken = requestToken;
        return this;
    }

    public String getKey() {
        return Key;
    }

    public String getSecret() {
        return Secret;
    }

    public String getCallbackURL() {
        return CallbackURL;
    }

    public OAuthToken getRequestToken() {
        return RequestToken;
    }

    public String getAuthorizeUrl() {
        return AuthorizeUrl;
    }

    public String getRequestUrl() {
        return RequestUrl;
    }

    public String getAccessUrl() {
        return AccessUrl;

    }
}
