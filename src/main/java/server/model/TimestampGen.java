package server.model;

import java.util.Random;

/**
 * Created by grigory on 11/8/16.
 */
public class TimestampGen {

    public long generateTimestamp() {
        return System.currentTimeMillis() / 1000;
    }

    public long generateNonce(long timestamp) {
        return timestamp + RANDOM.nextInt();
    }

    static final Random RANDOM = new Random();

}